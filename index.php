<html>
	<head>
		<link href="semantic/semantic.css" rel="stylesheet">
	</head>
	<body>
		<?php
			require('menu.php');
		?>
		<article class='ui stackable grid'>
			<div class="ui grid">
				<div class="one wide column"></div>
				<div class="fourteen wide column">
					<form action="processar.php" class='ui form' method='post'>
						<div class="two fields">
							<div class='ui field'>
							    <label>Usuário</label>
							    <input type="text" name="user" placeholder="Usuário">
							</div>
							<div class="field">
								<label>Senha</label>
							        <input type="password" name="senha" placeholder="Senha">
							</div>
						</div>
						<input type="submit" name="submit" class='ui button green right floated'>
					</form>
				</div>
				<div class="one wide column"></div>
			</div>
			
		</article>
	</body>
</html>